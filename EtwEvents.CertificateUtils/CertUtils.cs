﻿using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Formats.Asn1;
using System.Runtime.Versioning;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;

namespace KdSoft.EtwEvents
{
    /// <summary>
    /// Describes an element/attribute of a <see cref="X500RelativeDistinguishedName">Relative Distinguished Name</see>.
    /// </summary>
    /// <param name="Oid">Object Identifier for attribute.</param>
    /// <param name="Value">Attribute Value.</param>
    /// <param name="TagNo">Universal ASN.1 tag number for the attribute.</param>
    public record class RdnAttribute(string Oid, string Value, UniversalTagNumber? TagNo): IEquatable<RdnAttribute>
    {
        public bool Equals(RdnAttribute? x, RdnAttribute? y) => x?.Oid == y?.Oid && x?.Value == y?.Value;
        public int GetHashCode([DisallowNull] RdnAttribute obj) => obj.Oid.GetHashCode() ^ obj.Value.GetHashCode();
    }

    /// <summary>
    /// Describes a <see cref="X500RelativeDistinguishedName">Relative Distinguished Name</see>.
    /// </summary>
    /// <param name="Attributes">List of <see cref="RdnAttribute"/> instances making up the RDN.</param>
    public record class Rdn(IList<RdnAttribute> Attributes);

    public static class Oids
    {
        public const string ClientAuthentication = "1.3.6.1.5.5.7.3.2";
        public const string ServerAuthentication = "1.3.6.1.5.5.7.3.1";
        public const string EmailProtection = "1.3.6.1.5.5.7.3.4";
        public const string Role = "2.5.4.72";
        public const string CommonName = "2.5.4.3";
        public const string EmailAddress = "1.2.840.113549.1.9.1";
    }

    public static class CertUtils
    {
        #region Get Certificates

        /// <summary>
        /// Loads certificate from file, accepting multiple file types (PEM, Pkcs#12).
        /// </summary>
        /// <param name="filePath">Path to certificate file.</param>
        /// <param name="keyPath">Path to key file, if key is in separate file.</param>
        /// <param name="pwd">Password, if applicable.</param>
        /// <returns><see cref="X509Certificate2"/> instance.</returns>
        /// <exception cref="ArgumentException"></exception>
        /// <remarks>If the key is encrypted it must be in encrypted PKCS#8 format, with label 'ENCRYPTED PRIVATE KEY'.</remarks>
        public static X509Certificate2 LoadCertificateFromFile(string filePath, string? keyPath, string? pwd) {
            keyPath = string.IsNullOrEmpty(keyPath) ? null : keyPath;
            pwd = string.IsNullOrEmpty(pwd) ? null : pwd;

            X509ContentType contentType;
            try {
                contentType = X509Certificate2.GetCertContentType(filePath);
            }
            catch (CryptographicException) {
                contentType = X509ContentType.Unknown;
            }
            return contentType switch {
                // we assume it is a PEM certificate
                X509ContentType.Unknown => pwd is null
                    ? X509Certificate2.CreateFromPemFile(filePath, keyPath)
                    : X509Certificate2.CreateFromEncryptedPemFile(filePath, pwd, keyPath),
                X509ContentType.Cert => keyPath is not null
                    ? (pwd is null
                        ? X509Certificate2.CreateFromPemFile(filePath, keyPath)
                        : X509Certificate2.CreateFromEncryptedPemFile(filePath, pwd, keyPath))
                    : X509CertificateLoader.LoadPkcs12FromFile(filePath, pwd),
                X509ContentType.Pfx => X509CertificateLoader.LoadPkcs12FromFile(filePath, pwd, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable),
                _ => throw new ArgumentException($"Unrecognized certificate type in file: {filePath}"),
            };
        }

        public static X509Certificate2 LoadCertificate(ReadOnlySpan<byte> rawData, string? pwd, Encoding? encoding = null) {
            X509ContentType contentType;
            try {
                contentType = X509Certificate2.GetCertContentType(rawData);
            }
            catch (CryptographicException) {
                contentType = X509ContentType.Unknown;
            }
            switch (contentType) {
                // we assume it is a PEM certificate with the unencrypted private key included
                case X509ContentType.Unknown:
                    var charSpan = (encoding ?? Encoding.Default).GetString(rawData);
                    return X509Certificate2.CreateFromPem(charSpan, charSpan);
                case X509ContentType.Cert:
                    return X509CertificateLoader.LoadCertificate(rawData);
                case X509ContentType.Pfx:
                    return X509CertificateLoader.LoadPkcs12(rawData, pwd, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                default:
                    throw new ArgumentException($"Unrecognized certificate type.");
            }
        }

        /// <summary>
        /// Get certificates from certificate store based on subject common name and optional enhanced key usage.
        /// The resulting collection is unordered.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open already.</param>
        /// <param name="subjectCN">Subject common name to look for, comparison is not case sensitive.</param>
        /// <param name="ekus">Enhanced key usage identifiers, all of which the certificate must support.</param>
        /// <returns>Matching certificates, or an empty collection if none were found.</returns>
        public static IEnumerable<X509Certificate2> GetCertificates(this X509Store store, string subjectCN, params string[] ekus) {
            var certs = store.Certificates.Find(X509FindType.FindBySubjectName, subjectCN, true);
            foreach (var matchingCert in certs) {
                // X509NameType.SimpleName extracts CN from subject (common name)
                var cn = matchingCert.GetNameInfo(X509NameType.SimpleName, false);
                if (string.Equals(cn, subjectCN, StringComparison.InvariantCultureIgnoreCase)) {
                    var matchesEkus = true;
                    foreach (var oid in ekus) {
                        matchesEkus = matchesEkus && matchingCert.SupportsEnhancedKeyUsage(oid);
                    }
                    if (matchesEkus) {
                        yield return matchingCert;
                    }
                }
            }
        }

        /// <summary>
        /// Get certificates from certificate store based on subject common name and optional enhanced key usage.
        /// The resulting collection is unordered.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="subjectCN">Subject common name to look for, comparison is not case sensitive.</param>
        /// <param name="ekus">Enhanced key usage identifiers, all of which the certificate must support.</param>
        /// <returns>Matching certificates, or an empty collection if none were found.</returns>
        public static IEnumerable<X509Certificate2> GetCertificates(StoreName storeName, StoreLocation location, string subjectCN, params string[] ekus) {
            using var store = new X509Store(storeName, location);
            store.Open(OpenFlags.ReadOnly);
            return GetCertificates(store, subjectCN.Trim(), ekus);
        }

        /// <summary>
        /// Get certificates from certificate store based on application policy OID and predicate callback.
        /// The resulting collection is unordered.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="policyOID">Application policy OID to look for, e.g. Client Authentication (1.3.6.1.5.5.7.3.2). Required.</param>
        /// <param name="predicate">Callback to check certificate against a condition. Optional.</param>
        /// <returns>Matching certificates, or an empty collection if none are found.</returns>
        public static IEnumerable<X509Certificate2> GetCertificates(this X509Store store, string policyOID, Predicate<X509Certificate2>? predicate) {
            var certs = store.Certificates.Find(X509FindType.FindByApplicationPolicy, policyOID, true);
            if (certs.Count == 0 || predicate == null)
                return certs;
            return certs.Where(crt => predicate(crt));
        }

        /// <summary>
        /// Get certificates from certificate store based on application policy OID and predicate callback.
        /// The resulting collection is unordered.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="policyOID">Application policy OID to look for, e.g. Client Authentication (1.3.6.1.5.5.7.3.2). Required.</param>
        /// <param name="predicate">Callback to check certificate against a condition. Optional.</param>
        /// <returns>Matching certificates, or an empty collection if none are found.</returns>
        public static IEnumerable<X509Certificate2> GetCertificates(StoreName storeName, StoreLocation location, string policyOID, Predicate<X509Certificate2>? predicate) {
            if (policyOID.Length == 0)
                return Enumerable.Empty<X509Certificate2>();

            using var store = new X509Store(storeName, location);
            store.Open(OpenFlags.ReadOnly);
            return GetCertificates(store, policyOID, predicate);
        }

        /// <summary>
        /// Get certificate from certificate store based on thumprint or subject common name.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open already.</param>
        /// <param name="thumbprint">Certificate thumprint to look for. Takes precedence over subjectCN when both are specified.</param>
        /// <param name="subjectCN">Subject common name to look for. Must be matched in full, but without case-sensitivity.</param>
        /// <returns>Matching certificate, or <c>null</c> if none was found.
        /// If multiple certificates match, will return the certificate with the latest expiration date.</returns>
        public static X509Certificate2? GetCertificate(this X509Store store, string thumbprint, string subjectCN) {
            if (thumbprint?.Length == 0 && subjectCN?.Length == 0)
                return null;

            X509Certificate2? cert = null;

            // find matching certificate, use thumbprint if available, otherwise use subject common name (CN)
            if (thumbprint?.Length > 0) {
                var certs = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, true);
                if (certs.Count > 0)
                    cert = certs[0];
            }
            else {
                var certs = store.Certificates.Find(X509FindType.FindBySubjectName, subjectCN!, true);
                foreach (var matchingCert in certs) {
                    // X509NameType.SimpleName extracts CN from subject (common name)
                    var cn = matchingCert.GetNameInfo(X509NameType.SimpleName, false);
                    if (string.Equals(cn, subjectCN, StringComparison.InvariantCultureIgnoreCase)) {
                        if (cert == null)
                            cert = matchingCert;
                        // if there are multiple matching certificates, return the one with the latest expiration date
                        else if (matchingCert.NotAfter > cert.NotAfter)
                            cert = matchingCert;
                    }
                }
            }

            return cert;
        }

        /// <summary>
        /// Get certificate from certificate store based on thumprint or subject common name.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="thumbprint">Certificate thumprint to look for. Takes precedence over subjectCN when both are specified.</param>
        /// <param name="subjectCN">Subject common name to look for. Must be matched in full, but without case-sensitivity.</param>
        /// <returns>Matching certificate, or <c>null</c> if none was found.
        /// If multiple certificates match, will return the certificate with the latest expiration date.</returns>
        public static X509Certificate2? GetCertificate(StoreLocation location, StoreName storeName, string thumbprint, string subjectCN) {
            using var store = new X509Store(storeName, location);
            store.Open(OpenFlags.ReadOnly);
            return GetCertificate(store, thumbprint, subjectCN);
        }

        /// <summary>
        /// Get certificate from certificate store based on Subject Distinguished Name.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open already.</param>
        /// <param name="name">Subject distinguished name of certificate.</param>
        /// <returns>Matching certificate, or <c>null</c> if none was found.</returns>
        public static X509Certificate2? GetCertificate(this X509Store store, X500DistinguishedName name) {
            if (name == null)
                return null;

            X509Certificate2? cert = null;
            var certs = store.Certificates.Find(X509FindType.FindBySubjectDistinguishedName, name.Name, true);
            if (certs.Count > 0)
                cert = certs[0];
            return cert;
        }

        /// <summary>
        /// Get certificate from certificate store based on Subject Distinguished Name.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="name">Subject distinguished name of certificate.</param>
        /// <returns>Matching certificate, or <c>null</c> if none was found.</returns>
        public static X509Certificate2? GetCertificate(StoreLocation location, StoreName storeName, X500DistinguishedName name) {
            using var store = new X509Store(storeName, location);
            store.Open(OpenFlags.ReadOnly);
            return GetCertificate(store, name);
        }

        /// <summary>
        /// Get certificates from certificate store based on X509FindType.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="findType">By which criteria to search.</param>
        /// <param name="findValue">Criteria to evaluate.</param>
        /// <param name="validOnly">Only return valid certificates.</param>
        /// <returns>Matching certificates.</returns>
        public static X509Certificate2Collection GetCertificates(StoreLocation location, StoreName storeName, X509FindType findType, object findValue, bool validOnly) {
            using (var store = new X509Store(storeName, location)) {
                store.Open(OpenFlags.ReadOnly);
                return store.Certificates.Find(findType, findValue, validOnly);
            }
        }

        /// <summary>
        /// Get all certificate from certificate store.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <returns>All certificates in store.</returns>
        public static X509Certificate2Collection GetAllCertificates(StoreLocation location, StoreName storeName) {
            // find matching certificate, use thumbprint if available, otherwise use subject common name (CN)
            using (var store = new X509Store(storeName, location)) {
                store.Open(OpenFlags.ReadOnly);
                return store.Certificates;
            }
        }

        /// <summary>
        /// Get certificates from store that match the Subject CommonName, Subject Alternative DnsName,
        /// and Extended Key Usages (Ekus) of the given certificate.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open already.</param>
        /// <param name="certificate">X509Certificate2 to match.</param>
        /// <returns>Matching certificates.</returns>
        public static IEnumerable<X509Certificate2> GetMatchingCertificates(this X509Store store, X509Certificate2 certificate) {
            var subjectCN = certificate.GetNameInfo(X509NameType.SimpleName, false);
            var dnsName = certificate.GetNameInfo(X509NameType.DnsName, false).Trim();
            var ekus = certificate.GetEnhancedKeyUsages().Select(eku => eku.Value ?? "");

            var certs = GetCertificates(store, subjectCN, ekus.ToArray());
            foreach (var cert in certs) {
                var tempDnsName = cert.GetNameInfo(X509NameType.DnsName, false).Trim();
                if (string.Equals(tempDnsName, dnsName, StringComparison.InvariantCultureIgnoreCase)) {
                    yield return cert;
                }
            }
        }

        /// <summary>
        /// Get certificate from store that matches the Subject CommonName, Subject Alternative DnsName,
        /// and Extended Key Usages (Ekus) of the given certificate and expires later than the given time span
        /// before the given certificate's expiry time.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open already.</param>
        /// <param name="certificate">X509Certificate2 to match.</param>
        /// <param name="matchIfNotBefore">Time span within which the latest matching certificate is allowed
        /// to expire before the given certificate, to be chosen as a match.</param>
        public static X509Certificate2? GetMatchingCertificate(this X509Store store, X509Certificate2 certificate, TimeSpan matchIfNotBefore = default) {
            var matchingCerts = GetMatchingCertificates(store, certificate).ToList();
            if (matchingCerts.Count == 0) {
                return null;
            }

            // reverse comparision to sort in descending order
            var comparer = Comparer<DateTime>.Default;
            matchingCerts.Sort((x, y) => comparer.Compare(y.NotAfter, x.NotAfter));
            var maybeResult = matchingCerts[0];

            if ((certificate.NotAfter - maybeResult.NotAfter) <= matchIfNotBefore) {
                return maybeResult;
            }
            return null;
        }

        /// <summary>
        /// Get certificate from store that matches the Subject CommonName, Subject Alternative DnsName,
        /// and Extended Key Usages (Ekus) of the given certificate and expires later than the given time span
        /// before the given certificate's expiry time.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="certificate">X509Certificate2 to match.</param>
        /// <param name="matchIfNotBefore">Time span within which the latest matching certificate is allowed
        /// to expire before the given certificate, to be chosen as a match.</param>
        public static (X509Certificate2?, string?) GetMatchingCertificate(StoreLocation location, StoreName storeName, X509Certificate2 certificate, TimeSpan matchIfNotBefore = default) {
            using var store = new X509Store(storeName, location);
            store.Open(OpenFlags.ReadOnly);
            return (GetMatchingCertificate(store, certificate, matchIfNotBefore), store.Name);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Determines if the certificate is self signed.
        /// </summary>
        /// <param name="certificate">The <see cref="X509Certificate2"/> to check.</param>
        /// <returns><c>true</c> if the certificate is self signed, <c>false</c> otherwise.</returns>
        public static bool IsSelfSigned(this X509Certificate2 certificate) {
            var subjectRaw = certificate.SubjectName.RawData;
            var issuerRaw = certificate.IssuerName.RawData;
            return subjectRaw.SequenceEqual(issuerRaw);
        }

        /// <summary>
        /// Returns a certificate's enhanced key usages as Oids.
        /// </summary>
        /// <param name="cert">Certificate to check.</param>
        public static IEnumerable<Oid> GetEnhancedKeyUsages(this X509Certificate2 cert) {
            foreach (var ext in cert.Extensions) {
                var ekus = ext as X509EnhancedKeyUsageExtension;
                if (ekus is not null) {
                    foreach (var eku in ekus.EnhancedKeyUsages) {
                        yield return eku;
                    }
                }
            }
        }

        /// <summary>
        /// Checks if a certificate supports a given enhanced key usage.
        /// </summary>
        /// <param name="cert">Certificate to check.</param>
        /// <param name="oid">Oid identifier for enhanced key usage.</param>
        /// <remarks>If a certificate has no EKUs, then it supports all of them.</remarks>
        public static bool SupportsEnhancedKeyUsage(this X509Certificate2 cert, string oid) {
            var ekuCount = 0;
            foreach (var ext in cert.Extensions) {
                var ekus = ext as X509EnhancedKeyUsageExtension;
                if (ekus is not null) {
                    foreach (var eku in ekus.EnhancedKeyUsages) {
                        ekuCount++;
                        if (eku.Value == oid)
                            return true;
                    }
                }
            }
            // if no ekus are present then all are present
            return ekuCount == 0;
        }

        /// <summary>
        /// Get roles from certificate subject (role OID = 2.5.4.72).
        /// </summary>
        /// <param name="cert">Certificate whose subject to inspect.</param>
        /// <returns>List of roles.</returns>
        public static List<string> GetSubjectRoles(this X509Certificate2 cert) {
            var rdns = cert.SubjectName.GetRelativeNames();
            var result = new List<string>();
            //var roleOid = Oid.FromOidValue(OidRole, OidGroup.All); //this throws "The OID value is invalid" for some reason
            var roleOID = new Oid(Oids.Role);
            foreach (var rdn in rdns) {
                foreach (var att in rdn.Attributes) {
                    if (att.Oid == roleOID.Value)
                        result.Add(att.Value);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns <see cref="X509ChainPolicy"/> that can be used for client certificate validation.
        /// </summary>
        /// <param name="checkRevocation">Indicates if the certificate chain must be checked for revocation.
        ///     The default is <c>false</c>. Should not be turned on for self-signed certificates,
        ///     as they cannot be checked for revocation.</param>
        public static X509ChainPolicy CreateClientCertPolicy(bool checkRevocation = false) {
            var result = new X509ChainPolicy();
            // Enhanced Key Usage: Client Validation
            result.ApplicationPolicy.Add(Oid.FromOidValue(Oids.ClientAuthentication, OidGroup.EnhancedKeyUsage));
            if (!checkRevocation)
                result.RevocationMode = X509RevocationMode.NoCheck;
            return result;
        }

        /// <summary>
        /// Returns <see cref="X509ChainPolicy"/> that can be used for server certificate validation.
        /// </summary>
        /// <param name="checkRevocation">Indicates if the certificate chain must be checked for revocation.
        ///     The default is <c>false</c>. Should not be turned on for self-signed certificates,
        ///     as they cannot be checked for revocation.</param>
        public static X509ChainPolicy CreateServerCertPolicy(bool checkRevocation = false) {
            var result = new X509ChainPolicy();
            // Enhanced Key Usage: Server Validation
            result.ApplicationPolicy.Add(Oid.FromOidValue(Oids.ServerAuthentication, OidGroup.EnhancedKeyUsage));
            if (!checkRevocation)
                result.RevocationMode = X509RevocationMode.NoCheck;
            return result;
        }

        #endregion

        #region Private Keys

        const string NCRYPT_SECURITY_DESCR_PROPERTY = "Security Descr";
        const CngPropertyOptions NCRYPT_SILENT_FLAG = (CngPropertyOptions)0x00000040;
        const CngPropertyOptions DACL_SECURITY_INFORMATION = (CngPropertyOptions)0x00000004;

        public static AsymmetricAlgorithm? GetPrivateKey(X509Certificate2 cert, out bool isRSA) {
            const string RSA = "1.2.840.113549.1.1.1";
            const string ECC = "1.2.840.10045.2.1";
            isRSA = false;
            switch (cert.PublicKey.Oid.Value) {
                case RSA:
                    isRSA = true;
                    return cert.GetRSAPrivateKey();
                case ECC:
                    return cert.GetECDsaPrivateKey();
                default:
                    return null;
            }
        }

        public static CngKey? GetPrivateCngKey(this X509Certificate2 certificate) {
            var privateKeyAlgorithm = GetPrivateKey(certificate, out var isRsa);
            if (privateKeyAlgorithm is null)
                return null;

            CngKey privateKey;
            if (isRsa)
                privateKey = ((RSACng)privateKeyAlgorithm).Key;
            else
                privateKey = ((ECDsaCng)privateKeyAlgorithm).Key;
            return privateKey;
        }

        // This does not work: it produces an error "Key not valid for use in specified state." in privateKey.GetProperty()
        [Obsolete("This method does not work.")]
        [SupportedOSPlatform("windows")]
        public static void SetAccessRuleForPrivateKey(CngKey privateKey, FileSystemAccessRule accessRule) {
            FileSecurity fsec = new FileSecurity();
            fsec.AddAccessRule(accessRule);

            var prop = privateKey.GetProperty(NCRYPT_SECURITY_DESCR_PROPERTY, DACL_SECURITY_INFORMATION | NCRYPT_SILENT_FLAG);
            var binaryForm = prop.GetValue();
            if (binaryForm == null)
                throw new InvalidOperationException("CngKey.GetProperty returned null");
            fsec.SetSecurityDescriptorBinaryForm(binaryForm);

            var newProp = new CngProperty(prop.Name, fsec.GetSecurityDescriptorBinaryForm(), CngPropertyOptions.Persist | DACL_SECURITY_INFORMATION);
            privateKey.SetProperty(newProp);
        }

        // Here we are simply setting ACLs on the private key file directly
        [SupportedOSPlatform("windows")]
        public static bool SetAccessRuleForPrivateKeyFile(CngKey privateKey, FileSystemAccessRule accessRule) {
            var keyFileDir = GetKeyFileDirectory(privateKey);
            if (keyFileDir is null)
                return false;

            var keyFilePath = Path.Combine(keyFileDir, privateKey.UniqueName!);
            var keyFileInfo = new FileInfo(keyFilePath);

            var sectionFlags = AccessControlSections.Access;
            var fileSec = new FileSecurity(keyFilePath, sectionFlags);
            fileSec.AddAccessRule(accessRule);

            FileSystemAclExtensions.SetAccessControl(keyFileInfo, fileSec);
            return true;
        }

        /// <summary>
        /// Get access rules for the private key file of an RSA certificate.
        /// </summary>
        /// <param name="privateKey">CngKey instance.</param>
        /// <param name="useSid">Indicates if the Acl should use a SecurityIdentifier or NTAccount as identity reference.</param>
        /// <param name="includeInherited">Indicates if inherited Acls should be included.</param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <remarks>The Acl's identity reference can be translated from one type to the other with <see cref="IdentityReference.Translate"/>.</remarks>
        [SupportedOSPlatform("windows")]
        public static AuthorizationRuleCollection GetAccessRulesForPrivateKeyFile(CngKey privateKey, bool useSid = false, bool includeInherited = false) {
            var keyFileDir = GetKeyFileDirectory(privateKey);
            if (keyFileDir is null)
                throw new InvalidOperationException("Could not find private key file directory.");

            var keyFilePath = Path.Combine(keyFileDir, privateKey.UniqueName!);
            var keyFileInfo = new FileInfo(keyFilePath);

            var sectionFlags = AccessControlSections.Access;
            var fileSec = new FileSecurity(keyFilePath, sectionFlags);
            var targetType = useSid ? typeof(SecurityIdentifier) : typeof(NTAccount);
            return fileSec.GetAccessRules(true, includeInherited, targetType);
        }

        /// <summary>
        /// Get access rules for the private key file of a certificate.
        /// </summary>
        /// <param name="certificate">Certificate.</param>
        /// <param name="useSid">Indicates if the Acl should use a SecurityIdentifier or NTAccount as identity reference.</param>
        /// <param name="includeInherited">Indicates if inherited Acls should be included.</param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <remarks>The Acl's identity reference can be translated from one type to the other with <see cref="IdentityReference.Translate"/>.</remarks>
        [SupportedOSPlatform("windows")]
        public static AuthorizationRuleCollection GetAccessRulesForPrivateKey(this X509Certificate2 certificate, bool useSid = false, bool includeInherited = false) {
            var privateKey = GetPrivateCngKey(certificate);
            if (privateKey is null)
                throw new InvalidOperationException("The certificate does not have a private Key.");

            return GetAccessRulesForPrivateKeyFile(privateKey, useSid, includeInherited);
        }

        /// <summary>
        /// Add permissions to the private key file of a certificate.
        /// </summary>
        /// <param name="certificate">Certificate.</param>
        /// <param name="fileAcl">Instance of FileSystemAccessRule.</param>
        /// <returns><c>true</c> if the operation was successful, <c>false</c> otherwise (certificate has no private key).</returns>
        [SupportedOSPlatform("windows")]
        public static bool AddAccessRuleForPrivateKey(this X509Certificate2 certificate, FileSystemAccessRule fileAcl) {
            var privateKey = certificate.GetPrivateCngKey();
            if (privateKey is null)
                return false;
            return SetAccessRuleForPrivateKeyFile(privateKey, fileAcl);
        }

        /// <summary>
        /// Add permissions to the private key file of a certificate.
        /// </summary>
        /// <param name="certificate">Certificate</param>
        /// <param name="fsRights">FileSystemRights passed as int for portability.</param>
        /// <param name="accountName">NT account.</param>
        /// <returns><c>true</c> if the operation was successful, <c>false</c> otherwise (certificate has no private key).</returns>
        [SupportedOSPlatform("windows")]
        public static bool AddAccessRuleForPrivateKey(this X509Certificate2 certificate, int fsRights, string accountName) {
            var ntAccount = new NTAccount(accountName);
            var accessRule = new FileSystemAccessRule(ntAccount, (FileSystemRights)fsRights, AccessControlType.Allow);
            return AddAccessRuleForPrivateKey(certificate, accessRule);
        }

        /// <summary>
        /// Add permissions to the private key file of a certificate.
        /// </summary>
        /// <param name="certificate">Certificate</param>
        /// <param name="fsRights">FileSystemRights passed as int for portability.</param>
        /// <param name="sid">Security identifier.</param>
        /// <returns><c>true</c> if the operation was successful, <c>false</c> otherwise (certificate has no private key).</returns>
        [SupportedOSPlatform("windows")]
        public static bool AddAccessRuleForPrivateKey(this X509Certificate2 certificate, int fsRights, SecurityIdentifier sid) {
            var accessRule = new FileSystemAccessRule(sid, (FileSystemRights)fsRights, AccessControlType.Allow);
            return AddAccessRuleForPrivateKey(certificate, accessRule);
        }

        // From https://github.com/SebastiaanLubbers/WF_WCF_Samples/blob/master/WCF/Setup/FindPrivateKey/CS/Driver.cs
        // and https://learn.microsoft.com/en-us/windows/win32/seccng/key-storage-and-retrieval
        [SupportedOSPlatform("windows")]
        public static string? GetRSAKeyFileDirectory(string keyFileName) {
            // set up searching directory from All User profile
            string allUserProfile = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            string machineKeyDir = allUserProfile + "\\Microsoft\\Crypto\\RSA\\MachineKeys";

            // seach the key file
            var foundFile = Directory.EnumerateFiles(machineKeyDir, keyFileName).FirstOrDefault();
            if (foundFile is not null)
                return machineKeyDir;

            // next try seach directory for current user profile
            string currentUserProfile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string userKeyDir = currentUserProfile + "\\Microsoft\\Crypto\\RSA\\";

            // seach all sub directories
            var subDirectories = Directory.EnumerateDirectories(userKeyDir);
            foreach (string keyDir in subDirectories) {
                foundFile = Directory.EnumerateFiles(keyDir, keyFileName).FirstOrDefault();
                if (foundFile is not null)
                    return keyDir;
            }

            return null;
        }

        // From https://learn.microsoft.com/en-us/windows/win32/seccng/key-storage-and-retrieval
        [SupportedOSPlatform("windows")]
        public static string? GetCNGKeyFileDirectory(string keyFileName) {
            // set up searching directory from All User profile
            string allUserProfile = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            string sharedKeyDir = allUserProfile + "\\Microsoft\\Crypto\\Keys";

            // seach the key file
            var foundFile = Directory.EnumerateFiles(sharedKeyDir, keyFileName).FirstOrDefault();
            if (foundFile is not null)
                return sharedKeyDir;

            // next try seach directory for current user profile
            string currentUserProfile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string userKeyDir = currentUserProfile + "\\Microsoft\\Crypto\\Keys";

            foundFile = Directory.EnumerateFiles(userKeyDir, keyFileName).FirstOrDefault();
            if (foundFile is not null)
                return userKeyDir;

            return null;
        }

        /// <summary>
        /// Get the directory of the private key file of a certificate using the CNG API.
        /// </summary>
        /// <param name="privateKey"></param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <seealso cref="https://learn.microsoft.com/en-us/windows/win32/seccng/key-storage-and-retrieval" />
        [SupportedOSPlatform("windows")]
        public static string? GetKeyFileDirectory(CngKey privateKey) {
            // for the MicrosoftSoftwareKeyStorageProvider the CngKey.Uniquename is the file name
            if (privateKey.UniqueName == null)
                throw new InvalidOperationException("CngKey.UniqueName is null");
            if (privateKey.Provider == CngProvider.MicrosoftSoftwareKeyStorageProvider)
                return GetCNGKeyFileDirectory(privateKey.UniqueName);
            else
                return GetRSAKeyFileDirectory(privateKey.UniqueName);
        }

        #endregion

        #region Export to PEM

        static void ExportToPEM(X509Certificate2 cert, bool exportPrivateKey, StringBuilder builder) {
            if (exportPrivateKey) {
                var privateKey = GetPrivateKey(cert, out var isRSA);
                if (privateKey is not null) {
                    if (isRSA && privateKey is RSA rsaKey) {
                        builder.Append(rsaKey.ExportPkcs8PrivateKeyPem()).AppendLine();
                    }
                    else if (privateKey is ECDsa ecdsaKey) {
                        builder.Append(ecdsaKey.ExportPkcs8PrivateKeyPem()).AppendLine();
                    }
                }
            }
            builder.Append(cert.ExportCertificatePem());
        }

        /// <summary>
        /// Export a certificate to a PEM format string.
        /// </summary>
        /// <param name="cert">The certificate to export.</param>
        /// <returns>A PEM encoded string.</returns>
        public static string ExportToPEM(this X509Certificate2 cert, bool exportPrivateKey = true) {
            StringBuilder builder = new StringBuilder();
            ExportToPEM(cert, exportPrivateKey, builder);
            return builder.ToString();
        }

        /// <summary>
        /// Export multiple certificates to a PEM format string.
        /// </summary>
        /// <param name="certs">The certificates to export.</param>
        /// <returns>A PEM encoded string.</returns>
        public static string ExportToPEM(IEnumerable<X509Certificate2> certs, bool exportPrivateKey = true) {
            StringBuilder builder = new StringBuilder();
            foreach (var cert in certs) {
                ExportToPEM(cert, exportPrivateKey, builder);
            }
            return builder.ToString();
        }

        //        public static (string? publicKey, string? privateKey) ExportKeysToPEM(this X509Certificate2 cert) {
        //            var privateKey = GetPrivateKey(cert, out var isRsa);

        //            AsymmetricCipherKeyPair? kp = null;
        //            if (privateKey is RSA rsa) {
        //                kp = DotNetUtilities.GetRsaKeyPair(rsa);
        //            }
        //            else if (privateKey is DSA dsa) {
        //                kp = DotNetUtilities.GetDsaKeyPair(dsa);
        //            }
        //#if NET8_0_OR_GREATER
        //            else if (privateKey is ECDsa ecdsa) {
        //                kp = DotNetUtilities.GetECDsaKeyPair(ecdsa);
        //            }
        //#endif
        //            else if (privateKey == null) {
        //                throw new ArgumentException("Certificate does not have a private key.");
        //            }
        //            else {
        //                throw new ArgumentException("Private key type not supported.");
        //            }

        //            (string? publicKey, string? privateKey) result = (null, null);

        //            if (kp != null) {
        //                var sb = new StringBuilder();
        //                using (var tw = new StringWriter(sb)) {
        //                    var pw = new PemWriter(tw);

        //                    pw.WriteObject(kp.Public);
        //                    tw.Flush();
        //                    result.publicKey = sb.ToString();

        //                    sb.Clear();
        //                    pw.WriteObject(kp.Private);
        //                    tw.Flush();
        //                    result.privateKey = sb.ToString();
        //                }

        //            }
        //            return result;
        //        }

        #endregion

        #region Create Certificates

        // Many types are allowable.  We're only going to support the string-like ones
        // (This excludes IPAddress, X400 address, and other weird stuff)
        // https://www.rfc-editor.org/rfc/rfc5280#page-37
        // https://www.rfc-editor.org/rfc/rfc5280#page-112
        static readonly ImmutableHashSet<UniversalTagNumber> _allowedRdnTags = [
            UniversalTagNumber.TeletexString,
            UniversalTagNumber.PrintableString,
            UniversalTagNumber.UniversalString,
            UniversalTagNumber.UTF8String,
            UniversalTagNumber.BMPString,
            UniversalTagNumber.IA5String,
            UniversalTagNumber.NumericString,
            UniversalTagNumber.VisibleString,
            UniversalTagNumber.T61String
        ];

        /// <summary>
        /// Extracts the Relative Distinguished Names (RDNs) from a Distinguished Name (DN).
        /// Supports multi-valued RDNs. Does not support values other than string types.
        /// </summary>
        /// <param name="distinguishedName"></param>
        /// <returns>Collection of RDNS (Pairs of OID and Value).</returns>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="NotSupportedException"></exception>
        public static IEnumerable<Rdn> GetRelativeNames(this X500DistinguishedName distinguishedName) {
            var reader = new AsnReader(distinguishedName.RawData, AsnEncodingRules.DER);
            var snSeq = reader.ReadSequence();
            if (!snSeq.HasData) {
                throw new InvalidOperationException();
            }
            while (snSeq.HasData) {
                var rdnSet = snSeq.ReadSetOf();
                var rdnAttributes = new List<RdnAttribute>();
                while (rdnSet.HasData) {
                    var rdnSeq = rdnSet.ReadSequence();
                    while (rdnSeq.HasData) {
                        var attrOid = rdnSeq.ReadObjectIdentifier();
                        var attrValueTagNo = (UniversalTagNumber)rdnSeq.PeekTag().TagValue;
                        if (!_allowedRdnTags.Contains(attrValueTagNo)) {
                            throw new NotSupportedException($"Unknown tag type {attrValueTagNo} for attr {attrOid}");
                        }
                        var attrValue = rdnSeq.ReadCharacterString(attrValueTagNo);
                        rdnAttributes.Add(new RdnAttribute(attrOid, attrValue, attrValueTagNo));
                    }
                }
                yield return new Rdn(rdnAttributes);
            }
        }

        /// <summary>
        /// Writes relative distinguished names to an ASN.1 writer.
        /// </summary>
        /// <param name="rdns">Collection of <see cref="Rdn"/> instances.</param>
        /// <param name="writer"><see cref="AsnWriter"/> instance to use.</param>
        public static void WriteRelativeNames(IEnumerable<Rdn> rdns, AsnWriter writer) {
            using (writer.PushSequence()) {
                foreach (var rdn in rdns) {
                    using (writer.PushSetOf())
                        foreach (var att in rdn.Attributes) {
                            if (att.Oid is null) { continue; }
                            using (writer.PushSequence()) {
                                writer.WriteObjectIdentifier(att.Oid);
                                writer.WriteCharacterString(att.TagNo ?? UniversalTagNumber.UTF8String, att.Value);
                            }
                        }
                }
            }
        }

        public static void GetRelativeNameValues(this X500DistinguishedName distinguishedName, Oid oid, List<string> values) {
            var rdns = distinguishedName.GetRelativeNames();
            foreach (var rdn in rdns) {
                foreach (var att in rdn.Attributes) {
                    if (att.Oid == oid.Value) {
                        values.Add(att.Value);
                    }
                }
            }
        }

        public static string? GetRelativeNameValue(this X500DistinguishedName distinguishedName, Oid oid) {
            var result = new List<string>();
            var rdns = distinguishedName.GetRelativeNames();
            foreach (var rdn in rdns) {
                foreach (var att in rdn.Attributes) {
                    if (att.Oid == oid.Value) {
                        return att.Value;
                    }
                }
            }
            return null;
        }

        public static void AddServerExtensions(Collection<X509Extension> extensions, string? dnsName, PublicKey publicKey) {
            extensions.Add(new X509KeyUsageExtension(
                X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.NonRepudiation | X509KeyUsageFlags.KeyEncipherment,
                false
            ));
            extensions.Add(new X509EnhancedKeyUsageExtension(new OidCollection {
                Oid.FromOidValue(Oids.ServerAuthentication, OidGroup.EnhancedKeyUsage)
            }, false));

            if (!string.IsNullOrEmpty(dnsName)) {
                var sanBuilder = new SubjectAlternativeNameBuilder();
                sanBuilder.AddDnsName(dnsName);
                var sanExtension = sanBuilder.Build();
                extensions.Add(sanExtension);
            }

            extensions.Add(new X509BasicConstraintsExtension(false, false, 0, true));
            extensions.Add(new X509SubjectKeyIdentifierExtension(publicKey, false));
        }

        public static void AddClientExtensions(Collection<X509Extension> extensions, string? principalName, PublicKey publicKey) {
            extensions.Add(new X509KeyUsageExtension(
                X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment | X509KeyUsageFlags.DataEncipherment | X509KeyUsageFlags.KeyAgreement,
                false
            ));
            extensions.Add(new X509EnhancedKeyUsageExtension(new OidCollection {
                Oid.FromOidValue(Oids.ClientAuthentication, OidGroup.EnhancedKeyUsage),
                Oid.FromOidValue(Oids.EmailProtection, OidGroup.EnhancedKeyUsage)
            }, false));

            if (!string.IsNullOrEmpty(principalName)) {
                var sanBuilder = new SubjectAlternativeNameBuilder();
                sanBuilder.AddUserPrincipalName(principalName);
                var sanExtension = sanBuilder.Build();
                extensions.Add(sanExtension);
            }

            extensions.Add(new X509BasicConstraintsExtension(false, false, 0, true));
            extensions.Add(new X509SubjectKeyIdentifierExtension(publicKey, false));
        }

        /// <summary>
        /// Creates an asymmetric key compatible with a given issuer certificate.
        /// </summary>
        /// <param name="issuer">Certificate to derive key from.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static AsymmetricAlgorithm CreateAsymmetricKey(X509Certificate2 issuer) {
            using var issuerRSAKey = issuer.GetRSAPublicKey();
            if (issuerRSAKey is not null) {
                return RSA.Create();
            }
            using var issuerECDsaKey = issuer.GetECDsaPublicKey();
            if (issuerECDsaKey is not null) {
                return ECDsa.Create(issuerECDsaKey.ExportParameters(false).Curve);
            }
            throw new ArgumentException("Issuer certificate does not have an acceptable public key.", nameof(issuer));
        }

        /// <summary>
        /// Creates a certificate request for the given Subject Distinguished Name and asymmetric key.
        /// </summary>
        public static CertificateRequest CreateCertificateRequest(X500DistinguishedName subjectName, AsymmetricAlgorithm key) {
            if (key is RSA rsa)
                return new CertificateRequest(subjectName, rsa, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            else if (key is ECDsa ecdsa)
                return new CertificateRequest(subjectName, ecdsa, HashAlgorithmName.SHA384);
            throw new ArgumentException("Key is not of acceptable type.", nameof(key));
        }

        /// <summary>
        /// Creates a certificate from a given request and issuer.
        /// </summary>
        public static X509Certificate2 CreateCertificate(
            CertificateRequest request,
            X509Certificate2 issuer,
            DateTimeOffset startDate = default,
            ushort daysValid = 398
        ) {
            Span<byte> serialNumber = stackalloc byte[8];
            RandomNumberGenerator.Fill(serialNumber);

            if (startDate == default)
                startDate = DateTimeOffset.UtcNow;
            var notBefore = startDate < issuer.NotBefore ? issuer.NotBefore : startDate;

            return request.Create(issuer, notBefore, notBefore.AddDays(daysValid), serialNumber);
        }

        /// <summary>
        /// Creates a certificate usable for a server from a given request and issuer.
        /// </summary>
        public static X509Certificate2 CreateServerCertificate(CertificateRequest request, X509Certificate2 issuer, DateTimeOffset startDate = default, ushort daysValid = 398) {
            var subjectAltName = request.SubjectName.GetRelativeNameValue(Oid.FromFriendlyName("CN", OidGroup.Attribute));
            AddServerExtensions(request.CertificateExtensions, subjectAltName, request.PublicKey);
            return CreateCertificate(request, issuer, startDate, daysValid);
        }

        /// <summary>
        /// Creates a certificate usable for client authentication from a given request and issuer.
        /// </summary>
        public static X509Certificate2 CreateClientCertificate(CertificateRequest request, X509Certificate2 issuer, DateTimeOffset startDate = default, ushort daysValid = 398) {
            var email = request.SubjectName.GetRelativeNameValue(Oid.FromFriendlyName("email", OidGroup.Attribute));
            AddClientExtensions(request.CertificateExtensions, email, request.PublicKey);
            return CreateCertificate(request, issuer, startDate, daysValid);
        }

        #endregion

        #region Install Certificates

        /// <summary>
        /// Install the certificate in the LocalMachine scope, selecting the store based on the certificate type.
        /// </summary>
        /// <param name="certificate">The <see cref="X509Certificate2"/> to install.</param>
        public static void InstallMachineCertificate(X509Certificate2 certificate) {
            var storeName = StoreName.My;
            if (certificate.Extensions["2.5.29.19"] is X509BasicConstraintsExtension basicConstraintExt) {
                if (basicConstraintExt.CertificateAuthority) {
                    if (certificate.IsSelfSigned())
                        storeName = StoreName.Root;  // root CA
                    else
                        storeName = StoreName.CertificateAuthority;  // intermediate CA
                }
            }
            using var store = new X509Store(storeName, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadWrite);
            store.Add(certificate);
        }

        /// <summary>
        /// Install a certificate in a store, given its file path and password.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="certPath">Path to certificate file.</param>
        /// <param name="replace">Replace certificate if it exists.</param>
        /// <param name="pwd">Optional password for certificate private key.</param>
        /// <returns>Installed certificate and store name.</returns>
        public static (X509Certificate2?, string?) InstallCertificate(StoreLocation storeLocation, StoreName storeName, string certPath, string? keyPath, bool replace = true, string? pwd = null) {
            using (var store = new X509Store(storeName, storeLocation)) {
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);

                var certificate = LoadCertificateFromFile(certPath, keyPath, pwd);
                bool addCertificate = replace || !store.Certificates.Contains(certificate);
                if (addCertificate)
                    store.Add(certificate);

                return (certificate, store.Name);
            }
        }

        /// <summary>
        /// Install a certificate in a store, given its byte array and password.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="certData">Raw data of certificate.</param>
        /// <param name="replace">Replace certificate if it exists.</param>
        /// <param name="pwd">Optional password for certificate private key.</param>
        /// <returns>Installed certificate and store name.</returns>
        public static (X509Certificate2, string?) InstallCertificate(StoreLocation storeLocation, StoreName storeName, byte[] certData, bool replace = true, string? pwd = null) {
            using (var store = new X509Store(storeName, storeLocation)) {
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);

                var certificate = LoadCertificate(certData, pwd);
                // Certificates are compared by Issuer and SerialNumber
                bool addCertificate = replace || !store.Certificates.Contains(certificate);
                if (addCertificate)
                    store.Add(certificate);

                return (certificate, store.Name);
            }
        }

        /// <summary>
        /// Install a certificate in a store, or update a matching certificate if it expires at the same time or later.
        /// The actually used certificate will have both, the new private key file system access rules, and the access
        /// rules of the matching certificate, if any.
        /// </summary>
        /// <param name="store">X509Store instance. Must be open for read/write already.</param>
        /// <param name="certificate">X509Certificate2 to install, or match and update.</param>
        /// <param name="matchIfNotBefore">Time span within which the latest matching certificate is allowed
        /// to expire before the given certificate, to be chosen as a match.</param>
        /// <param name="fileSystemAccessRules">Access rules to be appplied to the certificate's private key.</param>
        /// <returns>The certificate actually installed or updated.</returns>
        [SupportedOSPlatform("windows")]
        public static X509Certificate2 InstallOrUpdateCertificate(
            this X509Store store,
            X509Certificate2 certificate,
            TimeSpan matchIfNotBefore = default,
            params FileSystemAccessRule[] fileSystemAccessRules
        ) {
            var matchingCert = GetMatchingCertificate(store, certificate, matchIfNotBefore);

            X509Certificate2 newCertificate;

            // if we won't use the matchingCertificate then we need to copy over the old ACLs
            if (matchingCert != null && certificate.NotAfter > matchingCert.NotAfter) {
                store.Add(certificate);

                var ntAccountRules = GetAccessRulesForPrivateKey(matchingCert);
                foreach (AuthorizationRule ntAccountRule in ntAccountRules) {
                    if (ntAccountRule is FileSystemAccessRule facl)
                        AddAccessRuleForPrivateKey(certificate, facl);
                }

                newCertificate = certificate;
            }
            else if (matchingCert == null) {
                store.Add(certificate);
                newCertificate = certificate;
            }
            else  // we are using the matchingCert if we found one
            {
                newCertificate = matchingCert;
            }

            // now we add the new ACLs
            foreach (var facl in fileSystemAccessRules) {
                AddAccessRuleForPrivateKey(newCertificate, facl);
            }

            return newCertificate;
        }

        /// <summary>
        /// Install a certificate in a store, or update a matching certificate if it already exists.
        /// </summary>
        /// <param name="location">Store location.</param>
        /// <param name="storeName">Store name.</param>
        /// <param name="certificate">X509Certificate2 to install, or match and update.</param>
        /// <param name="matchIfNotBefore">Time span within which the latest matching certificate is allowed
        /// to expire before the given certificate, to be chosen as a match.</param>
        /// <param name="fileSystemAccessRules">Access rules to be appplied to the certificate's private key.</param>
        /// <returns>The certificate actually installed or updated.</returns>
        [SupportedOSPlatform("windows")]
        public static (X509Certificate2, string?) InstallOrUpdateCertificate(
            StoreLocation storeLocation,
            StoreName storeName,
            X509Certificate2 certificate,
            TimeSpan matchIfNotBefore = default,
            params FileSystemAccessRule[] fileSystemAccessRules
        ) {
            using var store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);

            var usedCertificate = InstallOrUpdateCertificate(store, certificate, matchIfNotBefore, fileSystemAccessRules);
            return (usedCertificate, store.Name);
        }


        #endregion
    }
}
