﻿using System.Collections.Immutable;

namespace KdSoft.EtwEvents
{
    public class AggregatingNotifier
    {
        public AggregatingNotifier() {
            //
        }

        public void PostNotification(int index) {
            var changeEnumerators = Volatile.Read(ref _changeEnumerators);
            foreach (var enumerator in changeEnumerators) {
                try {
                    enumerator.Advance(index);
                }
                catch { }
            }
        }

        ImmutableList<ChangeEnumerator> _changeEnumerators = ImmutableList<ChangeEnumerator>.Empty;

        void AddEnumerator(ChangeEnumerator enumerator) {
            ImmutableInterlocked.Update(ref _changeEnumerators, ce => ce.Add(enumerator));
        }

        void RemoveEnumerator(ChangeEnumerator enumerator) {
            ImmutableInterlocked.Update(ref _changeEnumerators, ce => ce.Remove(enumerator));
        }

        public IAsyncEnumerable<int> GetNotifications() {
            return new ChangeListener(this);
        }

        // https://anthonychu.ca/post/async-streams-dotnet-core-3-iasyncenumerable/
        class ChangeListener: IAsyncEnumerable<int>
        {
            readonly AggregatingNotifier _changeNotifier;

            public ChangeListener(AggregatingNotifier changeNotifier) {
                this._changeNotifier = changeNotifier;
            }

            public IAsyncEnumerator<int> GetAsyncEnumerator(CancellationToken cancellationToken = default) {
                return new ChangeEnumerator(_changeNotifier, cancellationToken);
            }
        }

        // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/proposals/csharp-8.0/async-streams
        class ChangeEnumerator: PendingAsyncEnumerator
        {
            readonly AggregatingNotifier _changeNotifier;

            public ChangeEnumerator(AggregatingNotifier changeNotifier, CancellationToken cancelToken) : base(cancelToken) {
                this._changeNotifier = changeNotifier;
                changeNotifier.AddEnumerator(this);
            }

            public override ValueTask DisposeAsync() {
                _changeNotifier.RemoveEnumerator(this);
                return default;
            }
        }
    }
}
