﻿namespace KdSoft.EtwEvents
{
    /// <summary>
    /// An async enumerator that aggregates calls to <see cref="Advance"/> in such a way that <see cref="MoveNextAsync"/>
    /// will return a new iteration result as long as it was preceded by at least one call to <see cref="Advance"/>.
    /// If multiple calls to <see cref="Advance"/> are made after the last call to <see cref="MoveNextAsync"/> then
    /// the next call to <see cref="MoveNextAsync"/> will succeed once.
    /// </summary>
    public abstract class PendingAsyncEnumerator: IAsyncEnumerator<int>
    {
        readonly CancellationToken _cancelToken;
        readonly ManualResetValueTaskSource<bool> _vts;
        readonly object _taskSync = new();

        public PendingAsyncEnumerator(CancellationToken cancelToken) {
            this._cancelToken = cancelToken;
            this._vts = new ManualResetValueTaskSource<bool>();
            this._version = short.MinValue;  //  _vts.Version;

            cancelToken.Register(() => _vts.TrySetResult(false));
        }

        short _version;

        // can be  called from any thread anytime
        public void Advance(int indx) {
            Current = indx;

            lock (_taskSync) {
                if (_vts.Version != _version) {
                    _version = _vts.Version;
                    _vts.TrySetResult(true);
                }
            }
        }

        public int Current { get; private set; } = default!;

        public abstract ValueTask DisposeAsync();

        /// <summary>
        /// Like IEnumerator.MoveNext(), but must not be called concurrently with itself, or DisposeAsync().
        /// </summary>
        public ValueTask<bool> MoveNextAsync() {
            if (_cancelToken.IsCancellationRequested)
                return new ValueTask<bool>(false);

            lock (_taskSync) {
                if (_vts.Version == _version) {
                    _vts.TrySetResult(true);
                    _vts.Reset();
                }
                return new ValueTask<bool>(_vts, _vts.Version);
            }
        }
    }
}
